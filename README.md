# Malebolge

<strong>[EN]</strong>

Malebolge is a dark typeface inspired by the middle ages and by the current state of the world. If you use it on any of your projects, you must credit it as follows: "Typeface: Malebolge by Ariel Martín Pérez (tunera.xyz)" Thanks in advance.

Malebolge 1.0 contains a limited set of capital letters and some symbols. Please follow this project to keep updated about new glyphs.

Malebolge has been created by Ariel Martín Pérez (www.arielgraphisme.com - contact@arielgraphisme.com) and released under the SIL Open Font Licence 1.1 in 2020. Malebolge will be distributed by the Tunera Type Foundry (www.tunera.xyz).

To know how to use this typeface, please read the FAQ (http://www.tunera.xyz/f.a.q/)

<strong>[FR]</strong>

Malebolge est un caractère typographique obscur inspiré par le Moyen-Âge et par l'état actuel du monde. Si vous l'utilisez dans l'un de vos projets, vous devez le créditer comme suit : "Fonte: Malebolge par Ariel Martín Pérez (tunera.xyz)" Merci d'avance.

Malebolge a été créé par Ariel Martín Pérez (www.arielgraphisme.com - contact@arielgraphisme.com) et publié sous la licence SIL Open Font License 1.1 en 2020. Malebolge est distribué par Tunera Type Foundry (www.tunera.xyz).

Pour savoir comment utiliser cette fonte, veuillez lire la FAQ (http://www.tunera.xyz/fr/f.a.q/)

<strong>[ES]</strong>

Malebolge es un tipo de letra oscuro inspirado por la edad media y por el estado actual del mundo. Si lo utilizas en alguno de tus proyectos, deberás creditarlo de la siguiente manera: "Tipo de letra: Malebolge por Ariel Martín Pérez (tunera.xyz)" Gracias por adelantado.

Malebolge ha sido creado por Ariel Martín Pérez (www.arielgraphisme.com - contact@arielgraphisme.com) y publicado bajo la licencia SIL Open Font License 1.1 en 2020. Malebolge es distribuido por Tunera Type Foundry (www.tunera.xyz).

Para saber cómo usar este tipo de letra, lea las preguntas frecuentes (http://www.tunera.xyz/sp/f.a.q/)


## Specimen

![specimen1](documentation/specimen/malebolge-specimen-01.png)
![specimen1](documentation/specimen/malebolge-specimen-02.png)
![specimen1](documentation/specimen/malebolge-specimen-03.png)

## License

Malebolge is licensed under the SIL Open Font License, Version 1.1.
This license is copied as an attached file, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/unified-font-repository/Unified-Font-Repository
